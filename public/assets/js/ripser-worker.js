var Module = {};

var xhr = new XMLHttpRequest();
xhr.open('GET', '../emscripten/ripser.wasm', true);
xhr.responseType = 'arraybuffer';
xhr.onload = function (e) {
    Module.wasmBinary = xhr.response;
    importScripts('../emscripten/ripser.js');
    postMessage({type:"initialized"});
};
xhr.send(null);

addEventListener('message', function (e) {
    var data = e.data;
    if(Module.ripser_emscripten) {
        Module.ripser_emscripten(data.file, data.dim, data.threshold, data.format);
        postMessage({type:"finished"});
    } else {
        setTimeout(function () {
            Module.ripser_emscripten(data.file, data.dim, data.threshold, data.format);
            postMessage({type:"finished"});
        },100);
    }


}, false);
