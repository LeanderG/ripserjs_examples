import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid} from 'semantic-ui-react'

const id = "breast-cancer-wisconsin";

var runner;
export default class BreastCancer extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Diagnostic%29</div>
                            <div>Removed ID and Diagnosis column.</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(response.data, "#" + id, options)
    })
}