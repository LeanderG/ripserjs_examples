import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import circle2d from './images/circle_deleted.png'
export default class Circle2dDelete extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='circle2d'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={circle2d}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/circle2d_delete.csv").then(function (response) {
            Ripser.run(response.data, "#circle2d", {format: "point-cloud", maxDim: 3})
        })

    }
}