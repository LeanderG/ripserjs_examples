import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import circle3d from './images/circle.png'
export default class Circle3d extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='circle3d'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={circle3d}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/circle3d.csv").then(function (response) {
            Ripser.run(response.data, "#circle3d", {format: "point-cloud", maxDim: 3})
        })

    }
}