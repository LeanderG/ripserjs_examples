import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import circleStretched_01_2 from './images/circle_stretched_01_2.png'
export default class CircleStretched012 extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='circle-stretched-01-2'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={circleStretched_01_2}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/circle_stretched_01_2.csv").then(function (response) {
            Ripser.run(response.data, "#circle-stretched-01-2", {format: "point-cloud", maxDim: 1})
        })

    }
}