import React,{Component} from "react";
import { Grid, Image } from 'semantic-ui-react'
import Axios from 'axios'
import * as Ripser from "ripser";
import cube from './images/cube.png'
export default class Cube extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='cube'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={cube} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
    componentDidMount() {
        Axios.get("./assets/examples/cube.csv").then(function (response) {
            Ripser.run(response.data,"#cube",{format:"point-cloud",maxDim:3})
        })

    }
}