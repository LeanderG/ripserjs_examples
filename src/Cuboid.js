import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import cuboid from './images/cuboid.png'
export default class Cuboid extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='cuboid'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={cuboid} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
    componentDidMount() {
        Axios.get("./assets/examples/cuboid.csv").then(function (response) {
            Ripser.run(response.data,"#cuboid",{format:"point-cloud",maxDim:3})
        })

    }
}