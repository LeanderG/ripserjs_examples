import React, {Component} from "react";
import * as Ripser from "ripser";
import {Header, Tab, Container} from 'semantic-ui-react'

const panes = [
    {menuItem: 'Dimension 0', pane: {key: "tab1", content: <div id='tab1'></div>}},
    {menuItem: 'Dimension 1', pane: {key: "tab2", content: <div id='tab2'></div>}},
    {menuItem: 'Dimension 2', pane: {key: "tab3", content: <div id='tab3'></div>}},
]

const pointCloud = [
    [-1.1, -1.2, -0.9],
    [-1.01, -1.03, 1.1],
    [1.1, -1.2, -0.92],
    [1.2, -1.04, 1.1],
    [-1.1, 1.2, -0.9],
    [-1.2, 1.032, 1.1],
    [1.1, 1.2, 0.9],
    [1.12, 1.09, -1.1],
];

const codeString = "var options = {\n" +
    "  format: \"point-cloud\"\n" +
    "};\n" +
    "Ripser.run(pointCloud, [\"#info\",\"#tab1\",\"#tab2\",\"#tab3\"], options);"

export default class DomElementArrayTest extends Component {

    render() {
        console.log("renderd");
        return (
            <Container>

                <div id='info'></div>
                <Tab panes={panes} renderActiveOnly={false}/>
                <Header size='medium'>Source code:</Header>
                <div style={{'textAlign': 'left',"whiteSpace": "pre"}}>{codeString}
                </div>

            </Container>
        )
    }

    componentDidMount() {
        // TODO replace setTimeout hack with something better
        setTimeout(function () {
            var options = {
                format: "point-cloud"
            };
            Ripser.run(pointCloud, ["#info", "#tab1", "#tab2", "#tab3"], options);
        }, 50);
    }


}
