import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid} from 'semantic-ui-react'

const id = "HTRU_2_500";

var runner;
export default class HTRU2 extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>http://archive.ics.uci.edu/ml/datasets/HTRU2</div>
                            <div>Limited to the first 500 data points and removed the class attribute.</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(response.data, "#" + id, options)
    })
}