import React,{Component} from "react";
import { Dropdown, Menu,Container } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class Header extends Component {
    state = {}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    render() {
        const { activeItem } = this.state
        return (
            <Container>
            <Menu stackable>
                <Menu.Item
                    name='home'
                    active={activeItem === 'home'}
                    as={Link}
                    to='/'
                >
                    Home
                </Menu.Item>
                <Menu.Item
                    name='Test with your own data'
                    as={Link}
                    to='/interface'
                >
                    Test with your own data
                </Menu.Item>
                <Dropdown item text='Tests with synthetic data'>
                    <Dropdown.Menu>
                        <Dropdown.Item as={Link} to='/circle2d'>2D Circle</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/circle3d'>2D Circle in 3D Space</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/circle-stretched-01'>Circle stretched by 0.1</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/circle-stretched-01-more-points'>Circle stretched with twice as many points</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/tetrahedron'>Tetrahedron</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/cube'>Cube</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/cuboid'>Cuboid</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/icosphere'>Icosphere</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/torus'>Torus</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/two-circles'>Two Circles</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/one-and-two-clusters'>Different Data Sets have similar results</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/circle2d-missing'>2D Circle with missing point</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/circle2d-added'>2D Circle with a point added in the center</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown item text='Tests with real-world data'>
                    <Dropdown.Menu>
                        <Dropdown.Item as={Link} to='/seeds'>Seeds Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/stone-flakes'>StoneFlakes Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/wine'>Wine Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/hyg'>HYG Star Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/water-treatment'>Water Treatment Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/frogs'>Anuran Calls Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/HTRU2'>HTRU2 Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/breast-cancer-wisconsin'>Breast Cancer Wisconsin Data Set</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/auto-mpg'>Auto MPG</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/iris'>Iris Data Set</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown item text='Tests for Ripser.js features'>
                    <Dropdown.Menu>
                        <Dropdown.Item as={Link} to='/lower-distance-test'>Input data is a lower distance matrix as a 2D array</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/point-cloud-test'>Input data is an array of points</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/dom-element-array-test'>Each Dimension goes in a different DOM Element</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/out-of-memory-exception'>Fails with an out of memory exception</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/exporter'>Export to CSV</Dropdown.Item>
                        <Dropdown.Item as={Link} to='/hyg-heat-map'>HYG Heat Map</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>


                {/*<Menu.Item>*/}
                    {/*<Button primary>Run</Button>*/}
                {/*</Menu.Item>*/}
                {/*<Dropdown item>*/}
                {/*</Dropdown>*/}
            </Menu>
            </Container>
        )
    }
}