import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import icosphere from './images/ico_sphere.png'
export default class Icosphere extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='icosphere'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={icosphere}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/icosphere.csv").then(function (response) {
            Ripser.run(response.data, "#icosphere", {format: "point-cloud", maxDim: 3})
        })

    }
}