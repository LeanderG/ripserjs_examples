import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid, Button} from 'semantic-ui-react'

const id = "iris";

var runner;
export default class Seeds extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>
                                <Button onClick={runDefault} primary>Run default</Button>
                                <Button onClick={runNormalized} primary>Run normalized</Button>
                            </div>
                            <div>https://archive.ics.uci.edu/ml/datasets/Iris</div>
                            <div>Removed class column.</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(response.data, "#" + id, options)
    })
}

function runNormalized() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/${id}_normalized.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(response.data, "#" + id, options)
    })
}