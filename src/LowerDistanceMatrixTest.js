import React,{Component} from "react";
import * as Ripser from "ripser";
import { Grid, Header } from 'semantic-ui-react'

const
codeString = "var lowerDistanceMatrix = [\n" +
"  [1],\n" +
"  [3,1],\n" +
"  [4,3,1],\n" +
"  [3,4,3,1],\n" +
"  [1,3,4,3,1]\n" +
"];\n" +
"var options = {\n" +
"  outputType: \"Diagram\",\n" +
"  maxDim: 2\n" +
"};\n" +
"Ripser.run(lowerDistanceMatrix, \"#lower-dist\", options);"

export default class LowerDistanceMatrixTest extends Component {
    state = {
        lowerDistanceMatrix: [
            [1],
            [3,1],
            [4,3,1],
            [3,4,3,1],
            [1,3,4,3,1]
        ]
    }

    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={8}>
                            <div id='lower-dist'></div>

                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Header size='medium'>Source code:</Header>
                            <div style={{'textAlign':'left',"whiteSpace": "pre"}}>{codeString}</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        var options ={
            outputType: "PersistenceDiagram"
        };
        Ripser.run(this.state.lowerDistanceMatrix, "#lower-dist", options);

    }
}