import React, {Component} from "react";
import {Switch, Route} from 'react-router-dom'
import { Segment,Container } from 'semantic-ui-react'
import Home from './Home';
import Tests from './Tests';
import Cube from './Cube'
import Cuboid from './Cuboid'
import Icosphere from "./Icosphere";
import Tetrahedron from "./Tetrahedron";
import Torus from "./Torus";
import Circle2d from "./Circle2d";
import Circle3d from "./Circle3d";
import Seeds from "./Seeds";
import LowerDistanceMatrixTest from "./LowerDistanceMatrixTest";
import PointCloudTest from "./PointCloudTest";
import DomElementArrayTest from "./DomElementArrayTest";
import StoneFlakes from "./StoneFlakes";
import CircleStretched_01_2 from "./CircleStretched_01_2";
import CircleStretched_01 from "./CircleStretched01";
import Wine from "./Wine";
import HYG from "./hyg";
import OutOfMemoryException from "./OutOfMemoryException";
import Export from "./Export";
import HYGHeatMap from "./hygHeatMap";
import TwoCircles from "./twoCircles";
import WaterTreatment from "./WaterTreatment";
import SameResult from "./SameResult";
import Frogs from "./frogs";
import HTRU2 from "./HTRU2";
import BreastCancer from "./BreastCancer";
import AutoMPG from "./AutoMPG";
import Iris from "./Iris";
import Circle2dDelete from "./Circle2dDelete";
import Circle2dAdded from "./Circle2dAdded";
import Interface from "./interface";

export default class Main extends Component {
    render() {
        return (
            <Container>
                <Segment>
                <Switch>
                    <Route exact path='/' component={Home}/>

                    <Route path='/interface' component={Interface}/>

                    <Route path='/tests' component={Tests}/>
                    <Route path='/circle2d' component={Circle2d}/>
                    <Route path='/circle3d' component={Circle3d}/>
                    <Route path='/circle-stretched-01' component={CircleStretched_01}/>
                    <Route path='/circle-stretched-01-more-points' component={CircleStretched_01_2}/>
                    <Route path='/tetrahedron' component={Tetrahedron}/>
                    <Route path='/cube' component={Cube}/>
                    <Route path='/cuboid' component={Cuboid}/>
                    <Route path='/icosphere' component={Icosphere}/>
                    <Route path='/torus' component={Torus}/>
                    <Route path='/two-circles' component={TwoCircles}/>
                    <Route path='/one-and-two-clusters' component={SameResult}/>
                    <Route path='/circle2d-missing' component={Circle2dDelete}/>
                    <Route path='/circle2d-added' component={Circle2dAdded}/>

                    <Route path='/seeds' component={Seeds}/>
                    <Route path='/stone-flakes' component={StoneFlakes}/>
                    <Route path='/wine' component={Wine}/>
                    <Route path='/hyg' component={HYG}/>
                    <Route path='/water-treatment' component={WaterTreatment}/>
                    <Route path='/frogs' component={Frogs}/>
                    <Route path='/HTRU2' component={HTRU2}/>
                    <Route path='/breast-cancer-wisconsin' component={BreastCancer}/>
                    <Route path='/auto-mpg' component={AutoMPG}/>
                    <Route path='/iris' component={Iris}/>

                    <Route path='/lower-distance-test' component={LowerDistanceMatrixTest}/>
                    <Route path='/point-cloud-test' component={PointCloudTest}/>
                    <Route path='/dom-element-array-test' component={DomElementArrayTest}/>
                    <Route path='/out-of-memory-exception' component={OutOfMemoryException}/>
                    <Route path='/exporter' component={Export}/>
                    <Route path='/hyg-heat-map' component={HYGHeatMap}/>

                </Switch>
                </Segment>
            </Container>
        )
    }
}