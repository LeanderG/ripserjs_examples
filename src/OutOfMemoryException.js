import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid} from 'semantic-ui-react'

const id = "hygdata_v3_parsed";

export default class OutOfMemoryException extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id="out-of-memory"></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            This test will fail with an out of memory exception, because the number of data points and the dimension are too high.
                            Currently the only feedback is an error in the console.
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    document.getElementById("out-of-memory").innerHTML = '';
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        console.log(response)
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 3};
        Ripser.run(response.data, "#out-of-memory", options)
    })
}
