import React,{Component} from "react";
import * as Ripser from "ripser";
import { Grid, Header} from 'semantic-ui-react'

const  codeString = "var pointCloud = [\n" +
    "    [-1.1,-1.2,-0.9],\n" +
    "    [-1.01,-1.03,1.1],\n" +
    "    [1.1,-1.2,-0.92],\n" +
    "    [1.2,-1.04,1.1],\n" +
    "    [-1.1,1.2,-0.9],\n" +
    "    [-1.2,1.032,1.1],\n" +
    "    [1.1,1.2,0.9],\n" +
    "    [1.12,1.09,-1.1],\n" +
    "],\n" +
    "var options = {\n" +
    "    outputType: \"PersistenceDiagram\",\n" +
    "    format: \"point-cloud\",\n" +
    "    maxDim: 2\n" +
    "};\n" +
    "Ripser.run(pointCloud, \"#point-cloud\", options);"




export default class PointCloudTest extends Component {
    state = {
        pointCloud: [
            [-1.1, -1.2, -0.9],
            [-1.01, -1.03, 1.1],
            [1.1, -1.2, -0.92],
            [1.2, -1.04, 1.1],
            [-1.1, 1.2, -0.9],
            [-1.2, 1.032, 1.1],
            [1.1, 1.2, 0.9],
            [1.12, 1.09, -1.1],
        ]
    }


    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={8}>
                            <div id='point-cloud'></div>

                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Header size='medium'>Source code:</Header>
                            <div style={{'textAlign':'left',"whiteSpace": "pre"}}>{codeString}</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        var options ={
            outputType: "PersistenceDiagram",
            format: "point-cloud",
            maxDim: 2
        };
        Ripser.run(this.state.pointCloud, "#point-cloud", options);
    }
}
