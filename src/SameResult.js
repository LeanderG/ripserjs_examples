import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid, Button, Image} from 'semantic-ui-react'
import oneCluster from "./images/one-cluster.png"
import twoClusters from "./images/two-clusters.png"
var runner;
export default class SameResult extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id="clusters"></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>
                                <Button onClick={runOneCluster} primary>One Cluster</Button>
                                <div><Image src={oneCluster}/></div>
                                <Button onClick={runTwoClusters} primary>Two Clusters</Button>
                                <div><Image src={twoClusters}/></div>
                            </div>
                            <div>
                                Two very different data sets have a similar result.
                            </div>

                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runOneCluster()
    }
}

function runOneCluster() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/one-cluster.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 0};
        runner = new Ripser.run(response.data, "#clusters", options)
    })
}

function runTwoClusters() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/two-clusters.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 0};
        runner = new Ripser.run(response.data, "#clusters", options)
    })
}