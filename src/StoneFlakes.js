import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid, Button} from 'semantic-ui-react'


export default class StoneFlakes extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='stone-flakes'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>
                                <Button onClick={runDefault} primary>Run default</Button>
                                <Button onClick={runNormalized} primary>Run normalized</Button>
                            </div>
                            <div>http://archive.ics.uci.edu/ml/datasets/StoneFlakes</div>
                            <div>Removed points with missing data</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault();

    }
}

function runNormalized(){
    document.getElementById("stone-flakes").innerHTML = '';
    Axios.get("./assets/examples/StoneFlakes_normalized.csv").then(function (response) {
        var options = {format: "point-cloud", minDim: 1, maxDim: 3};
        Ripser.run(response.data, "#stone-flakes", options)
    })
}
function runDefault(){
    document.getElementById("stone-flakes").innerHTML = '';
    Axios.get("./assets/examples/StoneFlakes.csv").then(function (response) {
        var options = {format: "point-cloud", minDim: 1, maxDim: 3};
        Ripser.run(response.data, "#stone-flakes", options)
    })
}