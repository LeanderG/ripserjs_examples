import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import tetrahedron from './images/tetrahedron.png'
export default class Tetrahedron extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='tetrahedron'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={tetrahedron}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/tetrahedron.csv").then(function (response) {
            Ripser.run(response.data, "#tetrahedron", {format: "point-cloud", maxDim: 3})
        })

    }
}