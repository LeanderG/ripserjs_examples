import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import torus from './images/torus.png'

export default class Torus extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='torus'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={torus}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/torus.csv").then(function (response) {
            Ripser.run(response.data, "#torus", {format: "point-cloud", maxDim: 2})
        })

    }
}