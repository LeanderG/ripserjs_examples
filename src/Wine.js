import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Button } from 'semantic-ui-react'

export default class Wine extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='wine'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>
                                <Button onClick={runDefault} primary>Run default</Button>
                                <Button onClick={runNormalized} primary>Run normalized</Button>
                            </div>
                            <div>
                            http://archive.ics.uci.edu/ml/datasets/Wine
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault();
    }
}

function runNormalized(){
    document.getElementById("wine").innerHTML = '';
    Axios.get("./assets/examples/wine_normalized.csv").then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        Ripser.run(response.data, "#wine", options)
    })
}

function runDefault(){
    document.getElementById("wine").innerHTML = '';
    Axios.get("./assets/examples/wine.csv").then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        Ripser.run(response.data, "#wine", options)
    })
}
