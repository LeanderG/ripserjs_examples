import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid} from 'semantic-ui-react'

const id = "frogs";

var runner;
export default class Frogs extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>http://archive.ics.uci.edu/ml/datasets/Anuran+Calls+%28MFCCs%29</div>
                            <div>Only data for the species "HylaMinuta" is used.</div>
                            <div>Scatter plot: <a target="_blank" rel="noopener noreferrer" href="http://plnkr.co/edit/9ozcfGrT0o53DzW52Ojc">http://plnkr.co/edit/9ozcfGrT0o53DzW52Ojc</a></div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    if(runner) runner.stop(true);
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(response.data, "#" + id, options)
    })
}
