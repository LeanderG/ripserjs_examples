import React, {Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import {Grid} from 'semantic-ui-react'

const id = "hygdata_v3_parsed";

export default class HYG extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <div>https://github.com/astronexus/HYG-Database</div>
                            <div>Only using the x,y and z coordinates and the 1508 closest stars as seen from earth</div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        runDefault()
    }
}

function runDefault() {
    document.getElementById(id).innerHTML = '';
    Axios.get(`./assets/examples/${id}.csv`).then(function (response) {
        console.log(response)
        var options = {format: "point-cloud", outputType: "PersistenceDiagram", threshold: 8, minDim: 0, maxDim: 2};
        Ripser.run(response.data, "#" + id, options)
    })
}
