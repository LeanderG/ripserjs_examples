import React, {Component} from "react";
import * as Ripser from "ripser";
import {Button, Form, Grid, Radio} from 'semantic-ui-react'

const id = "custom";

var runner;
var data;

export default class Interface extends Component {
    state = {
        outputType: "PersistenceDiagram",
        format: "point-cloud",
        minDim: 1,
        maxDim: 2,
        threshold: Infinity,
    };
    handleFormatChange = (e, { value }) => this.setState({ format: value });
    handleOutputTypeChange = (e, { value }) => this.setState({ outputType: value });
    handleThresholdChange = (e) => this.setState({ threshold: e.target.value});
    handleMinDimChange = (e) => this.setState({ minDim: e.target.value });
    handleMaxDimChange = (e) => this.setState({ maxDim: e.target.value });


    openFile = function(files) {
        var reader = new FileReader();
        reader.onload = function(d){
            data = reader.result;
        };
        reader.readAsText(files[0]);
    };

    runDefault = () => {
        if (runner) runner.stop(true);
        console.log(this);
        var options = {
            outputType: this.state.outputType,
            format: this.state.format,
            minDim: parseFloat(this.state.minDim),
            maxDim: parseFloat(this.state.maxDim),
            threshold: parseFloat(this.state.threshold),
        };
        //var options = {format: "point-cloud", outputType: "PersistenceDiagram", minDim: 0, maxDim: 2};
        runner = new Ripser.run(data, "#" + id, options)
    };

    render() {
        return (
            <div style={{textAlign: "left"}}>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id={id}></div>
                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Form onSubmit={this.runDefault}>
                                <Form.Field>
                                    <label>Input File</label>
                                    <input type="file" onChange={ (e) => this.openFile(e.target.files) } />
                                </Form.Field>
                                <Form.Group grouped>
                                    <label>Input Format</label>
                                    <Form.Field>
                                        <Radio
                                            label='Lower Distance Matrix'
                                            name='formatRadioGroup'
                                            value='lower-distance'
                                            checked={this.state.format === 'lower-distance'}
                                            onChange={this.handleFormatChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label='Upper Distance Matrix'
                                            name='formatRadioGroup'
                                            value='upper-distance'
                                            checked={this.state.format === 'upper-distance'}
                                            onChange={this.handleFormatChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label='Distance Matrix'
                                            name='formatRadioGroup'
                                            value='distance'
                                            checked={this.state.format === 'distance'}
                                            onChange={this.handleFormatChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label='Point Cloud'
                                            name='formatRadioGroup'
                                            value='point-cloud'
                                            checked={this.state.format === 'point-cloud'}
                                            onChange={this.handleFormatChange}
                                        />
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group grouped>
                                    <label>Output Type</label>
                                    <Form.Field>
                                        <Radio
                                            label='Persistence Diagram'
                                            name='radioGroup'
                                            value='PersistenceDiagram'
                                            checked={this.state.outputType === 'PersistenceDiagram'}

                                            onChange={this.handleOutputTypeChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label='Persistence Barcodes'
                                            name='radioGroup'
                                            value='PersistenceBarCodes'
                                            checked={this.state.outputType === 'PersistenceBarCodes'}
                                            onChange={this.handleOutputTypeChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label='CSV Export'
                                            name='radioGroup'
                                            value='CSVExporter'
                                            checked={this.state.outputType === 'CSVExporter'}
                                            onChange={this.handleOutputTypeChange}
                                        />
                                    </Form.Field>
                                </Form.Group>
                                <Form.Field>
                                    <label>Threshold</label>
                                    <input
                                        value={this.state.threshold}
                                        onChange={this.handleThresholdChange }
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Minimum Dimension</label>
                                    <input
                                        value={this.state.minDim}
                                        onChange={this.handleMinDimChange }
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Maximum Dimension</label>
                                    <input
                                        value={this.state.maxDim}
                                        onChange={this.handleMaxDimChange }
                                    />
                                </Form.Field>
                                <Button type='submit'>Run</Button>
                            </Form>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

}
