import * as d3 from 'd3';

export var Ripser = (function() {

        function run(data, domElement, options) {

            //var scope = this;

            // private
            var defaultOptions = {
                diagramType: new RipserBarChart(),
                format: "lower-distance",
                minDim: 0,
                maxDim: 2,
                threshold: Infinity
            };
            var formatMap =
                {
                    "lower-distance": 0,
                    "upper-distance": 1,
                    "distance": 2,
                    "point-cloud": 3,
                    "dipha": 4
                };
            var _painter;
            var _data;
            var workerRunning = false;
            var timeoutAfter = 20000; // ms
            var _worker = new Worker("./assets/js/ripser-worker.js");

            _worker.addEventListener("message", handleMessage, false);
            setTimeout(() => {
                if(workerRunning) {
                    console.error("Ripser took too long. Terminating...");
                    _worker.terminate();
                }
                }, timeoutAfter)
            init();

            function init() {
                options = [defaultOptions, options].reduce(Object.assign, {});
                setPainter(options.diagramType);
                setData(data);
            }

            function setPainter(diagramType) {
                _painter = diagramType;

                if (Array.isArray(domElement)) {
                    if (domElement.length < options.maxDim - options.minDim + 1) {
                        throw new Error("Second argument has to be a selector or an array of selectors with length maxDim - minDim + 1");
                    }
                    _painter.domElement = domElement[0];
                } else {
                    _painter.domElement = domElement;
                }

            }

            function handleMessage(message) {
                //console.log(message);
                if (message.data === undefined) {
                    return;
                }
                var messageType = message.data.type;
                if (messageType === "initialized") {
                    workerRunning = true;
                    _worker.postMessage({
                        "file": _data,
                        "dim": options.maxDim,
                        "threshold": options.threshold,
                        "format": getFormatNumber(options.format)
                    });
                }
                else if (messageType === "finished") {
                    _worker.terminate();
                    workerRunning = false;
                }
                else if (messageType === "dim" && message.data.dim >= options.minDim) {
                    if (Array.isArray(domElement)) {
                        _painter.domElement = domElement[message.data.dim - options.minDim + 1];
                    }
                    _painter.initDiagram(this.valueRange, message.data.dim);

                } else if (messageType === "interval" && message.data.dim >= options.minDim) {

                    var dataPoint = {
                        "birth": message.data.birth,
                        "death": message.data.death ? message.data.death : this.valueRange[1],
                        "hasUpperBound": !!message.data.death
                    };

                    _painter.insertDataPoint(dataPoint)

                } else if (messageType === "distance-matrix") {
                    this.valueRange = [0, Math.min(message.data.max,options.threshold)];
                    this.distancesRange = [message.data.min, message.data.max];
                    _painter.valueRange = this.valueRange;
                    _painter.distancesRange = this.distancesRange;
                    if (_painter.headingRange == null) {
                        console.debug("Function headingRange(range) not defined on painter. The diagram will not output the range of distances from the distance matrix.");
                        return;
                    }
                    if (_painter.outputRange == null) {
                        console.debug("Function outputRange not defined on painter. The diagram will not output the range of distances from the distance matrix.")
                    }
                    _painter.outputRange();

                }
            }

            function setData(data) {
                if (typeof data === "string") {
                    _data = data;
                } else if (Array.isArray(data)) {
                    _data = matrixToString(data);
                } else {
                    throw new Error("Input data incorrectly formatted. Use a String or 2D-Array");
                }
            }

            function matrixToString(matrix) {
                return matrix.join("\r\n");
            }

            function getFormatNumber(format) {
                var number = formatMap[format];
                if(number === undefined) {
                    throw new Error(`Format '${format}' is not defined!`);
                }
                return number;
            }
        }

        function RipserBarChart() {


            // private
            var _data = [];
            var _index = [];

            var _x;
            var _y;
            var _svg;
            var _g;
            var _dimension;

            // public
            this.colorScheme = d3.schemeCategory10;
            this.barOutterHeight = 12;
            this.barInnerHeight = 4;
            this.margin = {top: 40, right: 20, bottom: 20, left: 20};
            this.width = 350;
            this.height = 350;

            this.headingPerDimension = function (dimension) {
                return '<h4>Persistence intervals in dimension ' + dimension + '</h4>';
            };

            this.headingRange = function (range) {
                var formattedRange = formatRange(range);
                return '<h4>Distances have the range [ ' + formattedRange[0] + ", " + formattedRange[1] + ' ]</h4>'
            };

            this.outputRange = function () {
                d3.select(this.domElement).append("div").html(this.headingRange(this.distancesRange));
            };

            this.initDiagram = function (newValueRange, newDimension) {
                _data = [];
                _index = [];
                this.valueRange = newValueRange;
                _dimension = newDimension;
                _x = d3.scaleLinear().domain(this.valueRange).range([0, this.width - this.margin.left - this.margin.right]);
                _y = d3.scaleBand()
                    .domain(_index)
                    .range([0, this.height - this.margin.top - this.margin.bottom])
                    .paddingInner(0.75)
                    .paddingOuter(0.25)
                    .round(.5);

                //insert heading
                d3.select(this.domElement).append("div").html(this.headingPerDimension(_dimension));

                _svg = d3.select(this.domElement).append("svg")
                    .attr("width", this.width)
                    .attr("height", 10);

                _svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
                    .call(d3.axisTop().scale(_x));

                _g = _svg.append("g")
                    .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + 10) + ")")
                    .attr("stroke", this.colorScheme[_dimension % 10]);

                _svg.append("defs")
                    .append("marker")
                    .attr("id", "arrow-" + _dimension)
                    .attr("viewBox", "0 0 12 12")
                    .attr("refX", 1)
                    .attr("refY", 5)
                    //.attr("markerUnits","strokeWidth")
                    .attr("markerWidth", 3)
                    .attr("markerHeight", 3)
                    .attr("orient", "auto")
                    .append("path")
                    .attr("d", "M 0 0 L 10 5 L 0 10 z")
                    .attr("fill", this.colorScheme[_dimension % 10]);


            };

            this.insertDataPoint = function (dataPoint) {
                _data.push(dataPoint);
                _index.push(_data.length - 1);
                _index.sort(function (a, b) {
                    return (_data[b].death - _data[b].birth - _data[a].death + _data[a].birth) || (a - b);
                });

                var height = this.barOutterHeight * _data.length;
                _y.domain(_index).range([0, height]);

                _svg//.transition().delay(50)
                    .attr("height", height + this.margin.top + 10 + this.margin.bottom);

                var enterSelection = _g.selectAll("polyline").data(_data)
                    .enter()
                    .append("polyline")
                    .attr("stroke-width", _y.bandwidth())
                    .attr("shape-rendering", "crispEdges")
                    .attr("marker-end", function (d) {
                        return d.hasUpperBound ? "" : "url(#arrow-" + _dimension + ")"
                    })
                    .append("title").html(function (d) {
                        return "[" + chop(d.birth).toString() + ",&thinsp;" + (d.hasUpperBound ? chop(d.death).toString() : "") + ")";
                    });

                _g.selectAll("polyline").merge(enterSelection)
                    .attr("points", function (d, i) {
                        return _x(d.birth) + "," + _y(i) + " " + _x(d.death) + "," + _y(i);
                    });

            };
        }

        function RipserDiagram() {

            // private
            var scope = this;
            var data;
            var x;
            var y;
            var svg;
            var g;
            var dimension;

            // public
            this.colorScheme = d3.schemeCategory10;

            this.margin = {top: 40, right: 20, bottom: 40, left: 60};
            this.width = 350;
            this.height = 350;
            this.opacity = 0.6;
            this.diameter = 12.0; // should be divisible by 2

            this.headingPerDimension = function (dimension) {
                return '<h4>Persistence intervals in dimension ' + dimension + '</h4>';
            };

            this.headingRange = function (range) {
                var formattedRange = formatRange(range);
                return '<h4>Distances have the range [ ' + formattedRange[0] + ", " + formattedRange[1] + ' ]</h4>'
            };

            this.outputRange = function () {
                d3.select(this.domElement).append("div").html(this.headingRange(this.valueRange));
            };

            this.initDiagram = function (newValueRange, newDimension) {
                dimension = newDimension;
                data = [];

                x = d3.scaleLinear().range([0, this.width - this.margin.left - this.margin.right]);

                y = d3.scaleLinear().range([this.height - this.margin.top - this.margin.bottom, 0]);
                y.domain(newValueRange);
                x.domain(newValueRange);

                //insert heading
                d3.select(this.domElement).append("div").html(this.headingPerDimension(dimension));

                svg = d3.select(this.domElement).append("svg")
                    .attr("width", this.width)
                    .attr("height", this.height);
                g = svg.append("g")
                    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

                // Add the X Axis
                g.append("g")
                    .attr("transform", "translate(0," + (this.height - this.margin.top - this.margin.bottom) + ")")
                    .call(d3.axisBottom(x));

                // Add the Y Axis
                g.append("g")
                    .call(d3.axisLeft(y));

                // Add dotted line
                g.append("line")
                    .attr("x1", 0)
                    .attr("y1", this.height - this.margin.top - this.margin.bottom)
                    .attr("x2", this.width - this.margin.left - this.margin.right)
                    .attr("y2", 0)
                    .attr("stroke-widht", 2)
                    .attr("stroke", "black")
                    .attr("stroke-dasharray", "5,5");
            };

            this.insertDataPoint = function (dataPoint) {
                data.push(dataPoint);

                var dataWithUpperBound = data.filter(function (t) {
                    return t.hasUpperBound
                });
                var dataWithoutUpperBound = data.filter(function (t) {
                    return !t.hasUpperBound
                });
                g.selectAll("circle")
                    .data(dataWithUpperBound)
                    .enter()
                    .append("circle")
                    .attr("cx", function (d) {
                        return x(d.birth);
                    })
                    .attr("cy", function (d) {
                        return y(d.death);
                    })
                    .attr("r", this.diameter/2 )
                    .attr("fill", this.colorScheme[dimension % 10])
                    .attr("fill-opacity",this.opacity)
                    .append("title").html(function (d) {
                    return "[" + chop(d.birth).toString() + ",&thinsp;" + chop(d.death).toString() + ")";
                });


                g.selectAll("rect")
                    .data(dataWithoutUpperBound)
                    .enter()
                    .append("rect")
                    .attr("x", function (d) {
                        return x(d.birth) - scope.diameter/2;
                    })
                    .attr("y", function (d) {
                        return y(d.death) - scope.diameter/2;
                    })
                    .attr("width", this.diameter)
                    .attr("height", this.diameter)
                    .attr("fill", this.colorScheme[dimension % 10])
                    .attr("fill-opacity",this.opacity)
                    .append("title").html(function (d) {
                    return "[" + chop(d.birth).toString() + ",&thinsp; )";
                });
            }
        }

        function RipserHeatmap() {
            this.initDiagram = function(newValueRange, newDimension) {

            }

            this.insertDataPoint = function(datapoint) {

            }
        }

        function chop(x) {
            return typeof x === "number" ? x.toPrecision(6) / 1 : x;
        }

        function formatRange(range) {


            var diff = range[1] - range[0];
            if(diff < 0.000000001)
                return range;
            var toFixed = Math.max(1, Math.log10(1 / diff) + 3);
            return [Number(range[0]).toFixed(toFixed), Number(range[1]).toFixed(toFixed)];
        }

        return {
            run: run,
            Diagram: RipserDiagram,
            BarChart: RipserBarChart,
            HeatMap: RipserHeatmap

        }
    }
)();