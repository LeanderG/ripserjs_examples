import React,{Component} from "react";
import Axios from "axios/index";
import * as Ripser from "ripser";
import { Grid, Image } from 'semantic-ui-react'
import twoCircles from './images/two_circles.png'
export default class TwoCircles extends Component {
    render() {
        return (
            <div>
                <Grid stackable divided='vertically'>
                    <Grid.Row columns={2}>
                        <Grid.Column width={11}>
                            <div id='two-circles'></div>

                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={twoCircles}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }

    componentDidMount() {
        Axios.get("./assets/examples/two_circles.csv").then(function (response) {
            Ripser.run(response.data, "#two-circles", {outputType:"PersistenceDiagram", format: "point-cloud", maxDim: 1})
        })

    }
}